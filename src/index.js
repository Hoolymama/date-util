
function dateOrNow(date) {
 if ( date instanceof Date) {
	 return date
	}
	return new Date()
}

exports.dateMinutesAgo = (minutes, date) => {
	const d = dateOrNow(date)
	return new Date(d.valueOf() - (minutes*60*1000))
}

exports.dateHoursAgo = (hours, date) => {
	return exports.dateMinutesAgo(hours*60, date)
}

exports.inMinutes = (minutes) => {
	return exports.dateMinutesAgo(-minutes)
}

exports.inHours = (hours) => {
	return exports.inMinutes(hours*60)
}

exports.randomDateBetween = (first, second) => {
	const min = first.getTime()
	const range =  (second.getTime() - min)+1
	return  new Date(Math.floor(Math.random()*range)+min)
}

exports.datesAreSince = (cutOffDate, ...dates) => {
	return !dates.some((date) => date <= cutOffDate)
}

exports.dateIsWithinTheLastMinutes = (date, cutOffMinutes) => {
	const cutOffDate = new Date(new Date().valueOf() - (cutOffMinutes  * 60 * 1000))
	return exports.datesAreSince(cutOffDate, date)
}

exports.dateIsWithinTheLastHours = (date, cutOffHours) => {
	const cutOffDate = new Date(new Date().valueOf() - (cutOffHours * 60 * 60 * 1000))
	return exports.datesAreSince(cutOffDate, date)
}

exports.ageInMinutes = (date, now) => {
	const dnow = dateOrNow(now)
	return Math.round((dnow - date)/(1000*60.0))
}

exports.roughAge = (date, now) => {
	let minutes = date 
	if (date instanceof Date) {
		minutes = exports.ageInMinutes(date, now)
	}

	if (minutes < 3) return "just now"
	if (minutes < 10) return `${minutes} mins`
	if (minutes < 50) return `${Math.round(minutes/5)*5} mins`
	if (minutes <= 180) return `${(Math.round(minutes/30)*0.5)} hrs`
	return `${(Math.round(minutes/60))} hrs`
}