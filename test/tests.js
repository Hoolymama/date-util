const expect = require("chai").expect
const  DateUtil = require("../src/index.js")

describe("Date Utils", () => {

	const epsilon = 1.0e-6

	let now
	beforeEach(function (){
		now = new Date()
	})

	it("should create a date a set number of minutes ago", function() {		
		const past = DateUtil.dateMinutesAgo(10)
		const minutes = Math.round((now.getTime()-past.getTime()) / (1000*60.0))
		expect(minutes).to.equal(10)
	})

	it("should create a date a set number of minutes in the future", function() {		
		future = DateUtil.inMinutes(10)
		const minutes = Math.round((future.getTime()-now.getTime()) / (1000*60.0))
		expect(minutes).to.equal(10)
	})

	it("should create a date a set number of minutes ago from a given date", function() {	
		const past = DateUtil.dateMinutesAgo(10, now)
		const minutes = Math.round((now.getTime()-past.getTime()) / (1000*60.0))
		expect(minutes).to.equal(10)
	})


	it("should create a date a set number of hours ago", function() {		
		const  past = DateUtil.dateHoursAgo(5)
		const hours = Math.round((now.getTime()-past.getTime()) / (1000*60*60.0))
		expect(hours).to.equal(5)
	})


	it("should create a date a set number of hours ago from a given date", function() {		
		const  past = DateUtil.dateHoursAgo(5, now)
		const hours = Math.round((now.getTime()-past.getTime()) / (1000*60*60.0))
		expect(hours).to.equal(5)
	})

	it("should create a date a set number of hours in the future", function() {		
		const future = DateUtil.inHours(5)
		const hours = Math.round((future.getTime()- now.getTime()) / (1000*60*60.0))
		expect(hours).to.equal(5)
	})

	it("should create a date a set number of fractional hours ago", function() {		
		const past = DateUtil.dateHoursAgo(5.25)
		const seconds = Math.round((now.getTime()-past.getTime()) / 1000)
		expect(seconds).to.equal(5.25*60*60)
	})

	it("should create a date a between 2 dates in the past", function() {		
		const first = DateUtil.dateHoursAgo(2) 
		const second = DateUtil.dateHoursAgo(1) 
		const rand = DateUtil.randomDateBetween(first, second)
		expect(rand-first).to.be.above(0)
		expect(second-rand).to.be.above(0)
	})

	it("should create a date a between 2 very close dates in the past", function() {		
		const first = DateUtil.dateHoursAgo(2) 
		const second = DateUtil.dateHoursAgo(1.99) 
		const rand = DateUtil.randomDateBetween(first, second)
		expect(rand-first).to.be.above(0)
		expect(second-rand).to.be.above(0)
	})

	it("should create a date a between 2 dates in the future", function() {		
		const first = DateUtil.inHours(2) 
		const second = DateUtil.inHours(3) 
		const rand = DateUtil.randomDateBetween(first, second)
		expect(rand-first).to.be.above(0)
		expect(second-rand).to.be.above(0)
	})

	it("should know if a date is recent or not", function() {		
		const cutOffDate = DateUtil.dateHoursAgo(2) 
		const recentDate = DateUtil.dateHoursAgo(1) 
		const staleDate = DateUtil.dateHoursAgo(3) 
		expect(DateUtil.datesAreSince(cutOffDate, recentDate)).to.be.equal(true)
		expect(DateUtil.datesAreSince(cutOffDate, staleDate)).to.be.equal(false)
	})

	it("should know dates are all recent", function() {	
		const cutOffDate = DateUtil.dateHoursAgo(2) 
		const d1 = DateUtil.dateHoursAgo(1) 
		const d2 = DateUtil.dateHoursAgo(1.5) 
		const d3 = DateUtil.dateHoursAgo(1.99) 
		expect(DateUtil.datesAreSince(cutOffDate, d1,d2,d3)).to.be.equal(true)
	})

	it("should know some dates are not recent", function() {	
		const cutOffDate = DateUtil.dateHoursAgo(2) 
		const d1 = DateUtil.dateHoursAgo(1) 
		const d2 = DateUtil.dateHoursAgo(1.5) 
		const d3 = DateUtil.dateHoursAgo(2.01) 
		expect(DateUtil.datesAreSince(cutOffDate, d1,d2,d3)).to.be.equal(false)
	})

	it("should compare date since minutes ago", function() {	
		expect(DateUtil.dateIsWithinTheLastMinutes(DateUtil.dateMinutesAgo(9),10)).to.be.equal(true)
		expect(DateUtil.dateIsWithinTheLastMinutes(DateUtil.dateMinutesAgo(11),10)).to.be.equal(false)
	})

	it("should compare date since hours ago", function() {	
		expect(DateUtil.dateIsWithinTheLastHours(DateUtil.dateHoursAgo(9),10)).to.be.equal(true)
		expect(DateUtil.dateIsWithinTheLastHours(DateUtil.dateHoursAgo(11),10)).to.be.equal(false)
	})

	it("should calculate age in minutes", function() {	
		expect(DateUtil.ageInMinutes(DateUtil.dateMinutesAgo(9))).to.be.equal(9)
		expect(DateUtil.ageInMinutes(DateUtil.dateMinutesAgo(137))).to.be.equal(137)
	})

	it("should provide rough age from Date object", function() {	
		expect(DateUtil.roughAge(DateUtil.dateMinutesAgo(2))).to.be.equal("just now")
		expect(DateUtil.roughAge(DateUtil.dateMinutesAgo(9))).to.be.equal("9 mins")
		expect(DateUtil.roughAge(DateUtil.dateMinutesAgo(43))).to.be.equal("45 mins")
		expect(DateUtil.roughAge(DateUtil.dateMinutesAgo(147))).to.be.equal("2.5 hrs")
		expect(DateUtil.roughAge(DateUtil.dateMinutesAgo(632))).to.be.equal("11 hrs")
	})

	it("should provide rough age from Date object when given a now date", function() {	
		expect(DateUtil.roughAge(DateUtil.dateMinutesAgo(43, now))).to.be.equal("45 mins")
	})

	it("should provide rough age from minutes", function() {	
		expect(DateUtil.roughAge(2)).to.be.equal("just now")
		expect(DateUtil.roughAge(9)).to.be.equal("9 mins")
		expect(DateUtil.roughAge(43)).to.be.equal("45 mins")
		expect(DateUtil.roughAge(147)).to.be.equal("2.5 hrs")
		expect(DateUtil.roughAge(632)).to.be.equal("11 hrs")
	})

		it("should provide rough age from minutes", function() {	
		expect(DateUtil.roughAge(43, now)).to.be.equal("45 mins")
	})
})
