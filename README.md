Date Utilities
=========

Conveniences to work with dates

## Installation

  `npm install @hoolymama/date-util`

## Usage

    var DateUtil = require("date-util")
    DateUtil.dateMinutesAgo(mins)
    DateUtil.inMinutes(mins)
    DateUtil.dateHoursAgo(hrs)
    DateUtil.inHours(hrs)
    DateUtil.randomDateBetween(date1,date2)
    DateUtil.datesAreSince(cutOffDate, ...dates)
    DateUtil.dateIsWithinTheLastMinutes(date, cutOffMinutes)
    DateUtil.dateIsWithinTheLastHours(date, cutOffHours)
    DateUtil.ageInMinutes(date)
    DateUtil.roughAge(date)
    DateUtil.roughAge(minutes)
    
## Tests

  `npm test`

## Contributing

You are sure to have better code style than me, so go for it.